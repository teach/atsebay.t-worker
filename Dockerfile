FROM alpine:3.16

RUN apk add --no-cache gawk git gnupg tar openssh-client-default xz && \
    adduser -h /work -D -u 1000 drone
