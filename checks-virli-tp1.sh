#!/bin/sh

STATUS=0

echo -ne "§ 1\tdocker-compose.yml\t\t\t"
VCP=$(find -maxdepth 1 -name "docker-compose.yml" | head -1)
[ -f "$VCP" ] || { echo "NOT FOUND"; STATUS=1; }
[ -f "$VCP" ] && echo "found "

exit $STATUS
